package com.frontado.sections.main;

import com.frontado.foundation.Presenter;

import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;

/**
 * Created by robertofrontado on 1/26/16.
 */
public class MainPresenter extends Presenter<MainView, GetTweetsUseCase> {

    @Inject
    public MainPresenter(GetTweetsUseCase useCase) {
        super(useCase);
    }

    @Override
    public void attachView(MainView view) {
        super.attachView(view);
        getTweets("");
    }

    public void getTweets(String query) {
        view.showProgress();

        useCase.setQuery(query);
        useCase.execute(new Subscriber<List<TweetEntity>>() {
            @Override
            public void onCompleted() {
                view.hideProgress();
            }

            @Override
            public void onError(Throwable e) {
                view.showErrorMessage(e.getMessage());
            }

            @Override
            public void onNext(List<TweetEntity> tweets) {
                view.showResult(tweets);
            }
        });
    }
}
