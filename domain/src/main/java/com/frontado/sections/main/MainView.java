package com.frontado.sections.main;

import com.frontado.foundation.BaseView;

import java.util.List;

/**
 * Created by robertofrontado on 1/26/16.
 */
public interface MainView extends BaseView {

    void showResult(List<TweetEntity> tweets);
    void showErrorMessage(String message);
    void showProgress();
    void hideProgress();
}
