package com.frontado.sections.main;

import lombok.Data;

/**
 * Created by robertofrontado on 1/26/16.
 */
@Data
public class TweetEntity {

    private final String text;
    private final String created_at;
    private final int retweet_count;
    private final UserEntity user;
}
