package com.frontado.sections.main;

import com.frontado.foundation.UseCase;
import com.frontado.foundation.schedulers.ObserveOn;
import com.frontado.foundation.schedulers.SubscribeOn;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by robertofrontado on 1/26/16.
 */
public class GetTweetsUseCase extends UseCase<TweetsRepository, List<TweetEntity>> {

    private String query;

    @Inject
    public GetTweetsUseCase(TweetsRepository repository, SubscribeOn subscribeOn, ObserveOn observeOn) {
        super(repository, subscribeOn, observeOn);
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public Observable<List<TweetEntity>> builtObservable() {
        return repository.getTweets(query);
    }
}
