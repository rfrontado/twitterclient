package com.frontado.sections.main;

import com.frontado.foundation.Repository;

import java.util.List;

import rx.Observable;

/**
 * Created by robertofrontado on 1/26/16.
 */
public interface TweetsRepository extends Repository {

    Observable<List<TweetEntity>> getTweets(String query);
}
