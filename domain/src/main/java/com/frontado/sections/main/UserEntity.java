package com.frontado.sections.main;

import lombok.Data;

/**
 * Created by robertofrontado on 1/30/16.
 */
@Data
public class UserEntity {

    private final String name;
    private final String screen_name;
    private final String profile_image_url;
}
