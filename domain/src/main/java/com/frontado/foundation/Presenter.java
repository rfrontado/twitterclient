package com.frontado.foundation;

/**
 * Created by robertofrontado on 1/26/16.
 */
public abstract class Presenter<V extends BaseView, U extends UseCase> implements Disposable {

    protected final U useCase;
    protected V view;

    public Presenter(U useCase) {
        this.useCase = useCase;
    }

    public void attachView(V view) {
        this.view = view;
    }

    @Override
    public void dispose() {
        useCase.dispose();
    }
}
