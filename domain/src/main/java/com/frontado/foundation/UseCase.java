package com.frontado.foundation;

import com.frontado.foundation.schedulers.ObserveOn;
import com.frontado.foundation.schedulers.SubscribeOn;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.Subscriptions;

/**
 * Created by robertofrontado on 1/26/16.
 */

public abstract class UseCase<R extends Repository, D> implements Disposable {
    protected final R repository;
    private final SubscribeOn subscribeOn;
    private final ObserveOn observeOn;
    private Subscription subscription = Subscriptions.empty();

    public UseCase(R repository, SubscribeOn subscribeOn, ObserveOn observeOn) {
        this.repository = repository;
        this.subscribeOn = subscribeOn;
        this.observeOn = observeOn;
    }

    public void execute(Subscriber<D> subscriber) {
        assert subscriber != null;

        unsubscribe();

        subscription = builtObservable()
                .subscribeOn(subscribeOn.getScheduler())
                .unsubscribeOn(subscribeOn.getScheduler())
                .observeOn(observeOn.getScheduler())
                .subscribe(subscriber);
    }

    public abstract Observable<D> builtObservable();

    private void unsubscribe() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override public void dispose() {
        unsubscribe();
    }
}