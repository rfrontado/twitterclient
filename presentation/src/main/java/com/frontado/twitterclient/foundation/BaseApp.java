package com.frontado.twitterclient.foundation;

import android.app.Application;

import com.frontado.twitterclient.di.ApplicationComponent;
import com.frontado.twitterclient.di.ApplicationModule;
import com.frontado.twitterclient.di.DaggerApplicationComponent;

/**
 * Created by robertofrontado on 1/26/16.
 */
public class BaseApp extends Application {
    private ApplicationComponent applicationComponent;

    @Override public void onCreate() {
        super.onCreate();
        initInject();
    }

    private void initInject() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
