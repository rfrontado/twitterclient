package com.frontado.twitterclient.foundation;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.frontado.twitterclient.R;
import com.frontado.twitterclient.di.ApplicationComponent;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by robertofrontado on 1/26/16.
 */
@EActivity(R.layout.activity_base)
public class BaseActivity extends AppCompatActivity {

    @ViewById
    protected Toolbar toolbar;

    public BaseApp getBaseApp() {
        return ((BaseApp)getApplication());
    }

    public ApplicationComponent getApplicationComponent() {
        return getBaseApp().getApplicationComponent();
    }

    @AfterInject
    protected void init() {
        getApplicationComponent().inject(this);
    }

    @AfterViews
    protected void initViews() {
        setSupportActionBar(toolbar);
    }

    protected void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}
