package com.frontado.twitterclient.sections.main;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.frontado.sections.main.MainPresenter;
import com.frontado.sections.main.MainView;
import com.frontado.sections.main.TweetEntity;
import com.frontado.twitterclient.R;
import com.frontado.twitterclient.foundation.BaseActivity;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import javax.inject.Inject;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity implements MainView {

    @Inject
    protected MainPresenter presenter;
    @ViewById
    RecyclerView recyclerView;

    private MainAdapter adapter;

    @Override protected void init() {
        super.init();
        getApplicationComponent().inject(this);
    }

    @Override
    protected void initViews() {
        super.initViews();
        setUpRecyclerView();

        presenter.attachView(this);
    }

    private void setUpRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MainAdapter();
        recyclerView.setAdapter(adapter);
    }

    @Override public void onDestroy() {
        super.onDestroy();
        presenter.dispose();
    }

    @Override
    public void showResult(List<TweetEntity> tweets) {
        adapter.setData(tweets);
    }

    @Override
    public void showErrorMessage(String message) {
        showToast(message);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }
}
