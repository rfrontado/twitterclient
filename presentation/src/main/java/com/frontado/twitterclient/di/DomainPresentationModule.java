package com.frontado.twitterclient.di;

import com.frontado.di.DomainModule;
import com.frontado.foundation.schedulers.ObserveOn;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by robertofrontado on 1/26/16.
 */
@Module(includes = {DomainModule.class, ApplicationModule.class})
public class DomainPresentationModule {

    /**
     * Provides the Scheduler which will be use for any Observable at the domain layer
     * to observeOn
     */
    @Singleton
    @Provides
    ObserveOn provideObserveOn() {
        return (() -> AndroidSchedulers.mainThread());
    }
}