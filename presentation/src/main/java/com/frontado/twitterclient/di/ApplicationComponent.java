package com.frontado.twitterclient.di;

import com.frontado.twitterclient.foundation.BaseActivity;
import com.frontado.twitterclient.sections.main.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by robertofrontado on 1/26/16.
 */
@Singleton @Component(modules = {DomainPresentationModule.class, DataPresentationModule.class, ApplicationModule.class})
public interface ApplicationComponent {

    void inject(BaseActivity baseActivity);
    void inject(MainActivity mainActivity);
}
