package com.frontado.twitterclient.di;

import com.frontado.di.DataModule;

import dagger.Module;

/**
 * Created by robertofrontado on 1/26/16.
 */
@Module(includes = {DataModule.class, ApplicationModule.class})
public class DataPresentationModule {


}