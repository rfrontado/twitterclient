package com.frontado.twitterclient.di;

import com.frontado.di.DataModule;
import com.frontado.twitterclient.foundation.BaseApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by robertofrontado on 1/26/16.
 */
@Module(includes = DataModule.class) public class ApplicationModule {
    private final BaseApp baseApp;

    public ApplicationModule(BaseApp baseApp) {
        this.baseApp = baseApp;
    }

    @Provides @Singleton BaseApp provideApplication() {
        return baseApp;
    }
}
