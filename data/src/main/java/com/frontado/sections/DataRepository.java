/*
 * Copyright 2015 RefineriaWeb
 */

package com.frontado.sections;

import com.frontado.foundation.Repository;

import com.frontado.net.RestApi;

import rx.Observable;

public abstract class DataRepository implements Repository {
    protected final RestApi restApi;

    public DataRepository(RestApi restApi) {
        this.restApi = restApi;
    }

    protected Observable buildObservableError(String message) {
        return Observable.create(subscriber -> subscriber.onError(new RuntimeException(message)));
    }

    protected <T> Observable<T> buildObservable(T t) {
        return Observable.just(t);
    }
}
