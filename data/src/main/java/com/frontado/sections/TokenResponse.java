package com.frontado.sections;

import lombok.Data;

/**
 * Created by robertofrontado on 1/30/16.
 */
@Data
public class TokenResponse {
    private final String token_type;
    private final String access_token;
}
