package com.frontado.sections.main;

import com.frontado.net.RestApi;
import com.frontado.sections.DataRepository;
import com.google.common.io.BaseEncoding;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by robertofrontado on 1/26/16.
 */
public class TweetsDataRepository extends DataRepository implements TweetsRepository {

    @Inject
    public TweetsDataRepository(RestApi restApi) {
        super(restApi);
    }

    @Override
    public Observable<List<TweetEntity>> getTweets(String query) {

            String basicAuthorization = "Basic " + BaseEncoding.base64().encode((restApi.CONSUMER_KEY + ":" + restApi.CONSUMER_SECRET).getBytes());

            return restApi.getBearerToken(basicAuthorization, "client_credentials")
                    .flatMap(tokenResponse -> {
                        try {
                            String bearerAuthorization = "Bearer " + tokenResponse.getAccess_token();
                            String queryEncoded = URLEncoder.encode("\"happy hour\" :)", "UTF-8");

                            return restApi.searchTweets(bearerAuthorization, queryEncoded)
                                    .map(jsonObject -> {
                                        //Get the tweetsArray at "statuses"
                                        JsonArray tweetsArray = jsonObject.getAsJsonArray("statuses");
                                        Type listType = new TypeToken<List<TweetEntity>>() {
                                        }.getType();
                                        //Parse the json
                                        List<TweetEntity> tweets = new GsonBuilder().create().fromJson(tweetsArray.toString(), listType);
                                        return tweets;
                                    })
                                    .flatMapIterable(tweetEntities -> tweetEntities)
                                    // Sort by retweets
                                    .toSortedList((tweetEntity, tweetEntity2) -> tweetEntity.getRetweet_count() > tweetEntity2.getRetweet_count() ? -1 : 1);

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                            throw new RuntimeException("Error at URLEncode");
                        }
                    });
    }

}
