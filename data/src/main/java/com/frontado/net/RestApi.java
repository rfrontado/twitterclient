package com.frontado.net;



import com.frontado.sections.TokenResponse;
import com.google.gson.JsonObject;

import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by robertofrontado on 1/26/16.
 */
public interface RestApi {

    String URL_BASE = "https://api.twitter.com";
    String CONSUMER_KEY = "o2TbgXOmVJMDW234OMhCBzJCt";
    String CONSUMER_SECRET = "bdnop2sMRFfHOcM7xPe1N08EWcr7QQ3vLp6bGU8QzRzbQwW2tY";

    @POST("/oauth2/token")
    Observable<TokenResponse> getBearerToken(@Header("Authorization") String authorization, @Query("grant_type") String grantType);

    @GET("/1.1/search/tweets.json")
    Observable<JsonObject> searchTweets(@Header("Authorization") String authorization, @Query("q") String query);
}