package com.frontado.di;

import com.frontado.net.RestApi;
import com.frontado.sections.main.TweetsDataRepository;
import com.frontado.sections.main.TweetsRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by robertofrontado on 1/26/16.
 */
@Module
public class DataModule {

    @Singleton @Provides RestApi provideRestApi() {
        return new Retrofit.Builder()
                .baseUrl(RestApi.URL_BASE)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(RestApi.class);
    }

    @Singleton @Provides TweetsRepository provideTweetsRepository(TweetsDataRepository tweetsDataRepository) {
        return tweetsDataRepository;
    }
}
